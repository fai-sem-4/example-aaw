/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iecompany.entity;

import com.opensymphony.xwork2.ActionSupport;


public class SurveyInfo extends ActionSupport {
    
    private int sid;
    private int age;
    private String product;
    private String country;
    private String gender;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    public SurveyInfo() {
    }
    
    public String execute() throws Exception {
        error = "Invalid age";
        if(age > 0 && age < 100) {
            return SUCCESS;   
        }else{
            return ERROR;        
        }
    }
    
}
